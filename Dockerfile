FROM python:3.7

WORKDIR /data

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      sox

RUN pip install \
      moviepy \
      deepspeech==0.5.1

ADD model.tar.gz /model

ENTRYPOINT ["deepspeech", "--model", "/model/output_graph_swiss.pb", "--alphabet", "/model/alphabet.txt", "--lm", "/model/LM.binary", "--trie", "/model/LM.trie", "--audio"]
